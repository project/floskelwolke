<?php

/**
 * Settings form for Floskelwolke.
 *
 * Simple Settings form to configure Floskelwolke.
 */
function floskelwolke_settings_form() {

  /*
  $form['notes'] = array(
    '#type' => 'markup',
    '#markup' => '',
  );
  */

  $form['fw_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Floskelwolke Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fw_settings']['fw_period_of_time'] = array(
    '#type' => 'select',
    '#title' => t('Floskelwolke display period of time'),
    '#options' => array(
      '0' => 'current data set',
      '86400' => 'last 24 hours',
      '172800' => 'last 48 hours',
      '259200' => 'last 36 hours'),
    '#default_value' => variable_get('fw_period_of_time', FLOSKELWOLKE_PERIOD_OF_TIME),
  );

  return system_settings_form($form);

}
