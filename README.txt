********************************************************************
                      D R U P A L    M O D U L E
********************************************************************
Name: Floskelwolke
Maintainer: http://medienverbinder.de
Written by Andre Langner
Making amazing Drupal based web applications for radio stations
and media companies is my passion. For further information or if
you have any questions please do not hesitate to contact me:

Visit my website: http://medienverbinder.de
Follow me on Twitter: http://twitter.com/medienverbinder
Visit my Drupal profile: https://www.drupal.org/u/medienverbinder
Drupal: 7
********************************************************************

INTRODUCTION
------------

Floskelwolke is an unofficial block module for the Floskelwolke API
to import ,display, analyze and process the CSV data. Floskelwoke.de
is a project of Udo Stiehl and Sebastian Pertsch.


INSTALLATION
------------

Libraries API 2

Download and install the Libraries API 2 module.
Move the downloaded module package to sites/all/modules/.
  => sites/all/modules/libraries

In administration backend on the module configuration (admin/modules)
page you have now to install/activate the module and save the module
configuration.


Floskelwolke Drupal Module
------------------------

Download and install the Floskelwolke module.
 Move the downloaded module package to:
=> sites/all/modules/floskelwolke

In administration backend on the module configuration (admin/modules)
page you have now to install/activate the module and save the module
configuration.


TagCanvas PlugIn (is required by fw_tagcanvas sub module)
---------------------------------------------------------

Download the TagCanvas plugin.
Make sure to use the TagCanvas plugin version 2.5.1.

Move the plugin to the directory "tagcanvas" and place it inside
  => "sites/all/libraries"

Make sure the path to the plugin file becomes:
  => "sites/all/libraries/tagcanvas/jquery.tagcanvas.min.js"

If you have Drush installed:

Download TagCanvas Library from project page:

  => drush fw-lib-tagcanvas


Notice:

The Drupal status report provides information on the correct installation
of the wordcloud Plugin. (/admin/reports/status)


wordcloud2.js plugin (is required by fw_wordcloud sub module)
-------------------------------------------------------------

Download the wordcloud2 plugin.

Move the plugin to the directory "wordcloud2" and place it inside
  => "sites/all/libraries"

Make sure the path to the plugin file becomes:
  => "sites/all/libraries/wordcloud2/src/wordcloud2.js"

If you have Drush installed:

Download wordcloud2 Library from project page:

  => drush fw-lib-wordcloud


Notice:

The Drupal status report provides information on the correct installation
of the wordcloud2 plugin. (/admin/reports/status)


Download and Import CSV Floskelwolke file
-----------------------------------------

The csv batch import can easily be started from the backend:

Import all Floskelwolke data:

  => /admin/config/floskelwolke/import (choose all)

Import latest Floskelwolke data:

  => /admin/config/floskelwolke/import (choose latest)

After importing the data can be viewed here:

  => /admin/config/floskelwolke/data


Activating and placing the TagCanvas/WordCloud block
------------------------------------------

After activation of the module a new block is added to the block configuration
page under structure / blocks. Now move the new block to the desired region and
click save.

The blocks are named as follows:

=> Floskelwolke WordCloud Block
=> Floskelwolke TagCanvas Block


The TagCanvas block can be modified with a few small parameters:

  => /admin/config/floskelwolke/tagcanvas/settings

The WorCloud block can be modified with a few small parameters:

  => /admin/config/floskelwolke/wordcloud/settings


CONTACT
-------

Making amazing Drupal based web applications for radio stations
and media companies is my passion. For further information or if
you have any questions please do not hesitate to contact me:

Visit my website: http://medienverbinder.de
Follow me on Twitter: http://twitter.com/medienverbinder
Visit my Drupal profile: https://www.drupal.org/u/medienverbinder
