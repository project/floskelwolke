<?php

/*
 * Implements hook_page().
 */
function floskelwolke_page() {

  $header = array(
    array('data' => t('Date'), 'field' => 'floskeltimestamp', 'sort' => 'asc'),
    array('data' => t('Phrase'), 'field' => 'floskeltitle', 'sort' => 'asc'),
    array('data' => t('Value'), 'field' => 'floskelvalue', 'sort' => 'asc'),
  );

  $selectfloskeln = db_select('floskelwolke', 'flw');
  $selectfloskeln->fields('flw', array(
    'floskeltimestamp',
    'floskeltitle',
    'floskelvalue')
  );

  // Add table sort extender.
  $table_sort = $selectfloskeln->extend('TableSort')
    ->orderByHeader($header); // Add order by headers.
  $pager = $table_sort->extend('PagerDefault')
    ->limit(15);
  $result = $pager-> execute();

  $arr = array();

  foreach ($result as $res) {

    $arr[] = array(
      date("d.m.Y - H:i", $res->floskeltimestamp),
      $res->floskeltitle,
      $res->floskelvalue,
    );
  };

  $output = FALSE;;

  // If rows are not empty theme and display the rows.
  if (!empty($arr)) {
    $output = theme('table', array(
      'header' => $header,
      'rows' => $arr,
      'attributes' => array(
        'floskeltimestamp' => 'sort-table',
        'floskelvalue' => 'sort-table',
      )
    ));
    $output .= theme('pager');
  }
  else {
    $output .= t("No Floskelwolke data found.");
  }

  return $output;
}
