<?php

/**
 * Import form for Floskelwolke.
 *
 * Simple form to upload Floskelwolke csv.
 */
function floskelwolke_form() {

  $form['notes'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="import-notes">A few notes when importing Floskelwolke CSV.<ul><li>Option <b>latest</b> imports only the latest published data.</li><li>The option <b>all</b> empty the floskelwolke table and imports all published data.</li></ul></div>',
  );

  $form['import'] = array(
    '#type' => 'select',
    '#title' => t('Import Floskelwolke CSV'),
    '#options' => array(
      'all' => 'all',
      'latest' => 'latest'),
    '#default_value' => 'latest',
  );

  $form['submit'] = array (
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;

}

/**
 *
 */
function floskelwolke_form_submit(&$form, &$form_state) {

  if ($form_state['input']['import'] == 'all') {

    $floskelwolkefile = FLOSKELWOLKE_ALL_CSV;
    // Truncate Floskelwolke DB-Table
    $result = db_truncate('floskelwolke')->execute();
    $uri = _floskelwolke_download_temporary($floskelwolkefile);
    $batch_operations = _floskelwolke_prepare_import_batch_all($uri);
    _floskelwolke_create_backend_batch($batch_operations);
  }

  if ($form_state['input']['import'] == 'latest') {
    $floskelwolkefile = FLOSKELWOLKE_LATEST_CSV;
    $uri = _floskelwolke_download_temporary($floskelwolkefile);
    $batch_operations = _floskelwolke_prepare_import_batch_latest($uri);
    _floskelwolke_create_backend_batch($batch_operations);
  }
}
