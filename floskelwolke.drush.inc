<?php

/**
 * @file
 * Floskelwolke - drush integration for floskelwolke.
 * Maintainer: http://medienverbinder.de
 * Written by Andre Langner
 */


/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function floskelwolke_drush_command() {

  $items = array();

  $items['fw-latest'] = array(
    'callback' => 'floskelwolke_drush_batch_latest',
    'description' => dt('Downloads the latest required csv data and start a batch process.'),
  );

  $items['fw-all'] = array(
    'callback' => 'floskelwolke_drush_batch_all',
    'description' => dt('Downloads the all csv data, empty the floskelwolke table and imports all published data.'),
  );

  return $items;
}


/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function floskelwolke_drush_help($section) {
  switch ($section) {
    case 'drush:fw-latest':
      return dt("Downloads the latest required csv data and start a batch process.");
    case 'drush:fw-all':
      return dt("Downloads the all csv data, empty the floskelwolke table and imports all published data.");
  }
}
