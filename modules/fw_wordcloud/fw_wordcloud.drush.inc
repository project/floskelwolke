<?php

/**
 * @file
 * Floskelwolke - drush integration for floskelwolke.
 * Maintainer: http://medienverbinder.de
 * Written by Andre Langner
 */

define('FW_WORDCLOUD_DOWNLOAD_URI', 'https://github.com/timdream/wordcloud2.js/archive/master.zip');
define('FW_WORDCLOUD_DOWNLOAD_URI_PREFIX', 'wordcloud2.js-');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function fw_wordcloud_drush_command() {

  $items = array();

  $items['fw-lib-wordcloud'] = array(
    'callback' => 'fw_wordcloud_drush_lib_download',
    'description' => dt('Downloads the required wordcloud2 plugin to library folder.'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function fw_wordcloud_drush_help($section) {
  switch ($section) {
    case 'drush:fw-lib-wordcloud':
      return dt("Downloads the required wordcloud2 plugin to library folder.");
  }
}

/**
 * Example drush command callback.
 *
 * This is where the action takes place.
 *
 * In this function, all of Drupals API is (usually) available, including
 * any functions you have added in your own modules/themes.
 *
 * To print something to the terminal window, use drush_print().
 */
function fw_wordcloud_drush_lib_download(){

  $path = 'sites/all/libraries';

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the tarball archive.
  if ($filepath = drush_download_file(FW_WORDCLOUD_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = FW_WORDCLOUD_DOWNLOAD_URI_PREFIX . basename($filepath, '.zip');

    // Remove any existing Geocomplete plugin directory.
    if (is_dir($dirname) || is_dir('wordcloud2')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('wordcloud2', TRUE);
      drush_log(dt('An existing Wordcloud plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    $result = drush_tarball_extract($filename);

    // Change the directory name to "geocomplete" if needed.
    if ($dirname != 'wordcloud2') {
      $result = drush_move_dir($dirname, 'wordcloud2', TRUE);
    }
  }

  if ($result) {
    drush_log(dt('Wordcloud2 plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Wordcloud2 plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
