<?php

/**
 * @file
 * HTML for Floskelwolke Word CLoud.
 */

?>

<div id="fw_wordcloud_div" style="width:100%;">
  <canvas id="fw_wordcloud_canvas" width="1655" height="795">
    <p>Anything in here will be replaced on browsers that support the canvas element</p>
  </canvas>
</div>
<h3 class="termsofuse"><a href="http://www.floskelwolke.de" target="_blank">Floskelwolke.de</a> is a project of Udo Stiehl and Sebastian Pertsch</h3>
<div id="tags">
</div>
