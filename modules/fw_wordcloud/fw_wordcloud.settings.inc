<?php

/**
 * @file
 * fw_wordcloud backend behavior configuration
 *
 * This file contains the settings and parameters
 * of the TagCavas plugin.
 * Maintainer: http://medienverbinder.de
 * Written by Andre Langner
 */


/**
 * Implements hook_form().
 */
function fw_wordcloud_settings_form($form, &$form_state) {

  $form['description'] = array(
    '#markup' => '<div>' . t('Select the relevant shape of your Floskelwolke WordCloud here.') . '</div>',
  );

  $form['floskelwolke_appearance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Floskelwolke Wordcloud Appearance'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['floskelwolke_appearance']['fw_wordcloud_shape'] = array(
    '#type' => 'select',
    '#title' => t('The shape of the WordCloud to draw.'),
    '#options' => array(
      'circle' => t('Circle'),
      'diamond' => t('Diamond'),
    ),
    '#default_value' => variable_get('fw_wordcloud_shape', FW_WORDCLOUD_DEFAULT_SHAPE),
    '#description' => t('Available WordCloud Shapes'),
  );

  $form['floskelwolke_appearance']['fw_wordcloud_clearcanvas'] = array(
    '#type' => 'select',
    '#title' => t('Clear Canvas'),
    '#options' => array(
      '1' => t('True'),
      '0' => t('False'),
    ),
    '#default_value' => variable_get('fw_wordcloud_clearcanvas', FW_WORDCLOUD_DEFAULT_CLEARCANVAS),
    '#description' => t('Paint the entire canvas with background color and consider it empty before start.'),
  );

  $form['floskelwolke_appearance']['fw_wordcloud_backgroundcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#description' => t('color of the background.'),
    '#default_value' => variable_get('fw_wordcloud_backgroundcolor', FW_WORDCLOUD_DEFAULT_BACKGROUNDCOLOR),
    '#size' => 40,
    '#maxlength' => 120,
  );

  $form['floskelwolke_appearance']['fw_wordcloud_abortthreshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Abort Threshold'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#size' => 4,
    '#maxlength' => 5,
    '#default_value' => variable_get('fw_wordcloud_abortthreshold', FW_WORDCLOUD_DEFAULT_ABORTTHRESHOLD),
    '#description' => t('If the call with in the loop takes more than x milliseconds (and blocks the browser), abort immediately.'),
  );

  $form['floskelwolke_appearance']['fw_wordcloud_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color'),
    '#description' => t('color of the text, can be any CSS color.'),
    '#default_value' => variable_get('fw_wordcloud_color', FW_WORDCLOUD_DEFAULT_COLOR),
    '#size' => 40,
    '#maxlength' => 120,
  );

  $form['#validate'][] = 'css_color_form_validate';
  return system_settings_form($form);

}


/**
 * Validation handler for css color.
 */
function css_color_form_validate($form, &$form_state) {

  $color = $form_state['values']['fw_wordcloud_color'];
  $backgroundcolor = $form_state['values']['fw_wordcloud_backgroundcolor'];
  
  if (!preg_match('/(?!\b)((#[0-9abcdef]{3}|#[0-9abcdef]{6})\b)/', $color)) {
      //form_set_error($element, t('Must be a valid hexadecimal CSS color value. Your value: %cssvalue',array('%cssvalue' => $element));
      form_set_error('fw_wordcloud_color',t('Must be a valid hexadecimal CSS color value. Your value: %cssvalue',array('%cssvalue' => $color)));
  }

  if (!preg_match('/(?!\b)((#[0-9abcdef]{3}|#[0-9abcdef]{6})\b)/', $backgroundcolor)) {
      //form_set_error($element, t('Must be a valid hexadecimal CSS color value. Your value: %cssvalue',array('%cssvalue' => $element));
      form_set_error('fw_wordcloud_backgroundcolor',t('Must be a valid hexadecimal CSS color value. Your value: %cssvalue',array('%cssvalue' => $backgroundcolor)));
  }
}