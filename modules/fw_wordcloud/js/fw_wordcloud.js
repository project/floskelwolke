(function ($) {
    Drupal.behaviors.fw_wordcloud = {
        attach: function(context, settings) {

          //alert("Drupal Behavior FLoskelwolke Wordcloud");

          var drupalfloskeln = Drupal.settings.fw_wordcloud.fw_wordcloud_floskeln;
          var floskeln = JSON.parse(drupalfloskeln);
          console.log("Drupal: " , drupalfloskeln);
          
          var fw_wordcloud_backgroundcolor = Drupal.settings.fw_wordcloud.fw_wordcloud_backgroundcolor;
          console.log('Backgroundcolor: ' + fw_wordcloud_backgroundcolor);

          var fw_wordcloud_shape = Drupal.settings.fw_wordcloud.fw_wordcloud_shape;
          console.log('Shape: ' + fw_wordcloud_shape);

          var fw_wordcloud_abortthreshold =  Number(Drupal.settings.fw_wordcloud.fw_wordcloud_abortthreshold);
          console.log('Abbort Threshold: ' + fw_wordcloud_abortthreshold);

          var fw_wordcloud_color = Drupal.settings.fw_wordcloud.fw_wordcloud_color;
          console.log('Color: ' + fw_wordcloud_color);

          var fw_wordcloud_clearcanvas = Drupal.settings.fw_wordcloud.fw_wordcloud_clearcanvas;
          console.log('Clearcanvas: ' + fw_wordcloud_clearcanvas);
          
          var div = document.getElementById("fw_wordcloud_div");
          var canvas = document.getElementById("fw_wordcloud_canvas");

          canvas.height = div.offsetHeight;
          canvas.width  = div.offsetWidth;

          //canvas.width  = window.innerWidth-25 || $(window).width()-25;
          //canvas.height = window.innerHeight-70 || $(window).height()-70;

            var options = { list: floskeln,
                gridSize: Math.round(12 * canvas.width / 1024),
                weightFactor: function (size) {
                    return Math.pow(size, 1.85) * canvas.width / 1024;
                },
                abortThreshold : fw_wordcloud_abortthreshold,
                fontFamily: 'Raleway',
                color: fw_wordcloud_color,
                fontWeight: 'bold',
                rotateRatio:0,
                backgroundColor: fw_wordcloud_backgroundcolor,
                shape: fw_wordcloud_shape,
                clearCanvas: fw_wordcloud_clearcanvas
            }
            WordCloud(document.getElementById('fw_wordcloud_canvas'), options);

        }
    }
})(jQuery);
