<?php

/**
 * @file
 * HTML for floskelwolke.
 *
 * @see template_preprocess_floskelwolke()
 */

//dpm($floskeln);

?>

<div id="myfloskelwolkediv">
  <canvas id="myfloskelwolkecanvas" width="<?php print check_plain($width) ?>" height="<?php print check_plain($height) ?>" style="width:100%;">
    <p>Anything in here will be replaced on browsers that support the canvas element</p>
  </canvas>
</div>
<h3 class="termsofuse"><a href="http://www.floskelwolke.de" target="_blank">Floskelwolke.de</a> is a project of Udo Stiehl and Sebastian Pertsch</h3>

<div id="tags">

  <?php

  if ($floskeln) :
    foreach ($floskeln as $item) :
      print $item;
    endforeach;
  endif;

  ?>

</div>
