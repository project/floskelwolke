<?php

/**
 * @file
 * Floskelwolke - drush integration for floskelwolke.
 * Maintainer: http://medienverbinder.de
 * Written by Andre Langner
 */

define('FW_TAGCANVAS_DOWNLOAD_URI', 'http://www.goat1000.com/jquery.tagcanvas.min.js?2.5.1');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function fw_tagcanvas_drush_command() {

  $items = array();
  $items['fw-lib-tagcanvas'] = array(
    'callback' => 'fw_tagcanvas_drush_lib_download',
    'description' => dt('Downloads the required tagcanvas plugin to library folder.'),
  );

  return $items;
}


/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function fw_tagcanvas_drush_help($section) {
  switch ($section) {
    case 'drush:fw-lib-tagcanvas':
      return dt("Downloads the required TagCanvas plugin to library folder.");
  }
}


/**
 * Example drush command callback.
 *
 * This is where the action takes place.
 *
 * In this function, all of Drupals API is (usually) available, including
 * any functions you have added in your own modules/themes.
 *
 * To print something to the terminal window, use drush_print().
 */
function fw_tagcanvas_drush_lib_download(){

  $path = 'sites/all/libraries/tagcanvas';
  $filename = "jquery.tagcanvas.min.js";

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  chdir($path);
  $path = $path."/".$filename;

  if (drush_download_file(FW_TAGCANVAS_DOWNLOAD_URI, $filename, TRUE)) {
    drush_op('chmod', $filename, 0644);
    drush_log(dt('TagCanvas plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the TagCanvas plugin to @path', array('@path' => $path)), 'error');
  }
}
