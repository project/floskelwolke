<?php

/**
 * @file
 * fw_tagcanvas backend behavior configuration
 *
 * This file contains the settings and parameters
 * of the TagCavas plugin.
 * Maintainer: http://medienverbinder.de
 * Written by Andre Langner
 */


define('FW_TAGCANVAS_DEFAULT_MOUSEWHEELZOOM', 'true');
define('FW_TAGCANVAS_DEFAULT_SHAPE', 'sphere');
define('FW_TAGCANVAS_DEFAULT_LINKSELECTION', 'true');
define('FW_TAGCANVAS_DEFAULT_WIDTH', '1600');
define('FW_TAGCANVAS_DEFAULT_HEIGHT', '1000');
define('FW_TAGCANVAS_DEFAULT_LOCK', 'null');
define('FW_TAGCANVAS_DEFAULT_MINSPEED', '0.01');
define('FW_TAGCANVAS_DEFAULT_MAXSPEED', '0.01');
define('FW_TAGCANVAS_DEFAULT_WEIGHTSIZE', '4.0');


/**
 * Implements hook_form().
 */
function fw_tagcanvas_settings_form($form, &$form_state) {

  $form['description'] = array(
    '#markup' => '<div>' . t('Select the relevant shape of your fw_tagcanvas here.') . '</div>',
  );

  $form['fw_tagcanvas_appearance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Floskelwolke TagCanvas Appearance'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fw_tagcanvas_appearance']['fw_tagcanvas_shape'] = array(
    '#type' => 'select',
    '#title' => t('Floskelwolke TagCanvas Shape'),
    '#options' => array(
      'sphere' => t('The sphere shape'),
      'vcylinder' => t('The vertical cylinder'),
      'hcylinder' => t('The horizontal cylinder'),
      'hring' => t('The horizontal ring'),
      'vring' => t('The vertical ring'),
    ),
    '#default_value' => variable_get('fw_tagcanvas_shape', FW_TAGCANVAS_DEFAULT_SHAPE),
    '#description' => t('Available TagCanvas Shapes'),
  );

  $form['fw_tagcanvas_appearance']['fw_tagcanvas_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Floskelwolke TagCanvas Width'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('Define canvas width in px.'),
    '#default_value' => variable_get('fw_tagcanvas_width', FW_TAGCANVAS_DEFAULT_WIDTH),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
  );

  $form['fw_tagcanvas_appearance']['fw_tagcanvas_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Floskelwolke TagCanvas Height'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('Define canvas height.'),
    '#default_value' => variable_get('fw_tagcanvas_height', FW_TAGCANVAS_DEFAULT_HEIGHT),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
  );

  $form['fw_tagcanvas_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Floskelwolke TagCanvas Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fw_tagcanvas_options']['fw_tagcanvas_mousewheelzoom'] = array(
    '#type' => 'select',
    '#title' => t('Floskelwolke TagCanvas Mouse Wheel Zoom'),
    '#options' => array(
      '1' => 'True',
      '0' => 'False'),
    '#default_value' => variable_get('fw_tagcanvas_mousewheelzoom', FW_TAGCANVAS_DEFAULT_MOUSEWHEELZOOM),
    '#description' => t('Enables zooming the cloud in and out using the mouse wheel or scroll gesture.'),
  );

  $form['fw_tagcanvas_options']['fw_tagcanvas_lock'] = array(
    '#type' => 'select',
    '#title' => t('Floskelwolke TagCanvas Lock'),
    '#options' => array(
      'null' => 'null',
      'x' => 'x',
      'y' => 'y',
      'xy' => 'xy',
    ),
    '#default_value' => variable_get('fw_tagcanvas_lock', FW_TAGCANVAS_DEFAULT_LOCK),
    '#description' => t('Limits rotation of the cloud using the mouse.'),
  );

  $form['fw_tagcanvas_options']['fw_tagcanvas_minspeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Floskelwolke TagCanvas MinSpeed'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('fw_tagcanvas_minspeed', FW_TAGCANVAS_DEFAULT_MINSPEED),
    '#description' => t('Minimum speed of rotation when mouse leaves canvas.'),
  );

  $form['fw_tagcanvas_options']['fw_tagcanvas_maxspeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Floskelwolke TagCanvas MaxSpeed'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('fw_tagcanvas_maxspeed', FW_TAGCANVAS_DEFAULT_MAXSPEED),
    '#description' => t('Maximum speed of rotation.'),
  );

  $form['fw_tagcanvas_options']['fw_tagcanvas_weightsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Floskelwolke TagCanvas Weight Size'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('fw_tagcanvas_weightsize', FW_TAGCANVAS_DEFAULT_WEIGHTSIZE),
    '#description' => t('The font size is multiplied by this number.'),
  );


  $form['#validate'][] = 'fw_tagcanvas_settings_form_validate';
  return system_settings_form($form);

}

/**
 * Implements hook_form_validate().
 */
function fw_tagcanvas_settings_form_validate($form, &$form_state) {
  $string_minspeed = $form_state['values']['fw_tagcanvas_minspeed'];
  if (_validate_float($string_minspeed, 0, 1)) {
    form_set_error('fw_tagcanvas_minspeed', t('minSpeed invalid characters. Only float are allowed! (max: 1.0 | min: 0)'));
  }
  $string_maxspeed = $form_state['values']['fw_tagcanvas_maxspeed'];
  if (_validate_float($string_maxspeed, 0, 1)) {
    form_set_error('fw_tagcanvas_maxspeed', t('maxSpeed invalid characters. Only float are allowed! (max: 1.0 | min: 0)'));
  }
}