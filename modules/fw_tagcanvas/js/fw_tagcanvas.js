/**
 * Defines the TagCanvas Block
 */
(function ($) {
    Drupal.behaviors.fw_tagcanvas = {
        attach: function(context, settings) {

            var tagcanvas_option_shape = Drupal.settings.fw_tagcanvas.fw_tagcanvas_shape;
            console.log('Shape: ' + tagcanvas_option_shape);

            var tagcanvas_option_mousewheelzoom = Number(Drupal.settings.fw_tagcanvas.fw_tagcanvas_mousewheelzoom);
            console.log('Mouse Wheel Zoom: ' + tagcanvas_option_mousewheelzoom);

            var tagcanvas_option_lock = Drupal.settings.fw_tagcanvas.fw_tagcanvas_lock;
            console.log('Lock: ' + tagcanvas_option_lock);

            var tagcanvas_option_minspeed = Drupal.settings.fw_tagcanvas.fw_tagcanvas_minspeed;
            console.log('Min Speed: ' + tagcanvas_option_minspeed);

            var tagcanvas_option_maxspeed = Drupal.settings.fw_tagcanvas.fw_tagcanvas_maxspeed;
            console.log('Max Speed: ' + tagcanvas_option_maxspeed);

            var tagcanvas_option_weightsize = Drupal.settings.fw_tagcanvas.fw_tagcanvas_weightsize;
            console.log('Weight Size: ' + tagcanvas_option_weightsize);

            if(!$('#myfloskelwolkecanvas').tagcanvas({

                    shape:     tagcanvas_option_shape,
                    wheelZoom: tagcanvas_option_mousewheelzoom,
                    lock:      tagcanvas_option_lock,
                    minSpeed:  tagcanvas_option_minspeed,
                    maxSpeed:  tagcanvas_option_maxspeed,
                    initial: [0.2,0.1],
                    textColour: null,
                    outlineColour: '#ff0000',
                    reverse: true,
                    depth: 0.8,
                    weight: true,
                    zoom: 0.8,
                    zoomMax: 1.2,
                    zoomMin: 0.5,
                    weightMode: 'both',
                    weightFrom: 'data-weight',
                    weightSize: tagcanvas_option_weightsize
                },'tags')) {
                // something went wrong, hide the canvas container
                $('#myfloskelwolkecanvas').hide();

                var div = document.getElementById("myfloskelwolkediv");
                var canvas = document.getElementById("myfloskelwolkecanvas");
                canvas.height = div.offsetHeight;
                canvas.width  = div.offsetWidth;

                canvas.width  = window.innerWidth || $(window).width();
                canvas.height = window.innerHeight || $(window).height();
            }
        }
    }
})(jQuery);